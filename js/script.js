// Відповіді на питання:

// 1. Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

// У try ... catch обертають код, у якому потенційно може статися помилка виконання, через отримання неліквідних даних (отримання даних із сервера, наприклад, завантаження файлів користувачем тощо).

// Конструкцію try...catch і throw - використовують, коли нам необхідно самостійно згенерувати помилки/винятки

const root = document.querySelector('#root');
const validationValue = ['author', 'name', 'price'];
const books = [{
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  }
];

function validation(item) {
  for (const value of validationValue) {
    try {
      if (!item[value]) {
        throw new Error(`${value} not found`);
      }
    } catch (error) {
      console.log(error.message);
      return false;
    }
  }
  return true;
}

function setBooks(items) {
  const booksContainer = document.createElement('ul');
  booksContainer.classList.add('books');
  root.append(booksContainer);

  items.forEach((item) => {
    const itemContainer = document.createElement('li');
    itemContainer.classList.add('books__item');

    for (const value of validationValue) {
      const bookValue = document.createElement('div');
      bookValue.classList.add(value);
      bookValue.innerText = `${value} : ${item[value]}`;

      itemContainer.append(bookValue);
      booksContainer.append(itemContainer);
    }
  });
}

function getBooks(books) {
  const result = [];
  books.forEach((item) => {
    if (validation(item)) {
      result.push(item);
    }
  });
  setBooks(result);
}

getBooks(books);